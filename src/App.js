import React from 'react';
import {} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import { About, Home, Splash, Version } from './pages';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const App = () => {
  const DrawerRouter = () => {
    return (
      <Drawer.Navigator
      drawerStyle={{backgroundColor:'#f8bbd0', padding:10,}}>
        <Drawer.Screen name="Home" component={Home}/>
        <Drawer.Screen name="About" component={About}  options={{headerShown:true}}/>
        <Drawer.Screen name="Version" component={Version}  options={{headerShown:true}}/>
      </Drawer.Navigator>
    )
  }
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Splash" component={Splash} options={{headerShown: false}}
        />
        <Stack.Screen
          name="Home" component={DrawerRouter} options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default App;
