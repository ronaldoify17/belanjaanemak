import React from 'react';
import {
  View,
  StatusBar,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
  Text,
  Alert,
  StyleSheet,
} from 'react-native';

const Home = () => {
  const [kata, setKata] = React.useState('');
  const [belanja, setBelanja] = React.useState([]);
  const ListItem = ({belanja}) => {
    return (
      <View
        style={styles.inputbelanja}>
        <View style={{flex: 1}}>
          <Text
            style={styles.textinput}
            style={{
              textDecorationLine: belanja?.completed ? 'line-through' : 'none',}}>
            {belanja?.task}
          </Text>
        </View>
        {!belanja?.completed && (
          <TouchableOpacity
            onPress={() => belanjaSelesai(belanja?.id)}
            style={styles.done}>
            <Image
              source={require('../../assets/images/done.png')}
              style={{width: 18, height: 18}}
            />
          </TouchableOpacity>
        )}
        <TouchableOpacity
          onPress={() => hapusBelanja(belanja?.id)}
          style={styles.hapus}>
          <Image
            source={require('../../assets/images/x.png')}
            style={{width: 15, height: 15}}
          />
        </TouchableOpacity>
      </View>
    );
  };
  const tambahBelanja = () => {
    if (kata == '') {
      Alert.alert('Error, Tolong input barang');
    } else {
      const belanjaBaru = {
        id: Math.random(),
        task: kata,
        completed: false,
      };
      setBelanja([...belanja, belanjaBaru]);
      setKata('');
    }
  };
  const belanjaSelesai = belanjaId => {
    const belanjaBaru = belanja.map(item => {
      if (item.id == belanjaId) {
        return {...item, completed: true};
      }
      return item;
    });
    setBelanja(belanjaBaru);
  };
  const hapusBelanja = belanjaId => {
    const belanjaBaru = belanja.filter(item => item.id != belanjaId);
    setBelanja(belanjaBaru);
  };
  const hapusSemuaBelanja = () => {
    Alert.alert('Yakin?', 'Hapus semua belanjaan?', [
      {
        text: 'Iya nih',
        onPress: () => setBelanja([]),
      },
      {text: 'Tidak'},
    ]);
  };
  return (
    <View style={{flex: 1, backgroundColor: '#f8bbd0'}}>
      <StatusBar backgroundColor="#ff80ab" barStyle="light-content" />
      <View
        style={{
          flexDirection: 'row',
          height: 60,
        }}>
        <View
          style={styles.menu}>
          {/* <TouchableOpacity>
            <Image
              source={require('../home/barsset.png')}
              style={{height: 35, width: 35}}
            />
          </TouchableOpacity> */}
        </View>
        <View
          style={styles.logo}>
            <Image
              source={require('../../assets/images/emak.png')}
              style={{height: 40, width: 125}}
            />
        </View>
        <View
          style={styles.hapusall}>
          <TouchableOpacity onPress={hapusSemuaBelanja}>
            <Image
              source={require('../../assets/images/delete.png')}
              style={{height: 35, width: 35}}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View>
        <TextInput
          placeholder="Cari.."
          placeholderTextColor="#9e9e9e"
          style={styles.cari}
        />
        <TouchableOpacity
          style={styles.tombolcari}
        >
          <Image 
            source={require('../../assets/images/search2.png')}
            style={{
              height:30,
              width:30,
          }}/>
        </TouchableOpacity>
      </View>
      <FlatList
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{padding: 20}}
        data={belanja}
        renderItem={({item}) => <ListItem belanja={item} />}
      />
      <View
        style={{
          flexDirection: 'row',
          bottom: 0,
          position: 'absolute',
        }}>
        <TextInput
          placeholder="Tambahkan belanjaan"
          placeholderTextColor="#9e9e9e"
          value={kata}
          onChangeText={text => setKata(text)}
          style={styles.textbelanja}
        />
        <TouchableOpacity onPress={tambahBelanja}>
          <View
            style={styles.plus}>
            <Image
              source={require('../../assets/images/plus.png')}
              style={{width: 35, height: 35}}
            />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  inputbelanja :{
    padding: 20,
    backgroundColor: 'white',
    elevation: 3,
    marginVertical: 7,
    borderRadius: 10,
    flexDirection: 'row',
  },
  textinput:{
    fontFamily: 'segoeprb',
    fontSize: 15,
    color: 'black',
  },
  done :{
    width: 25,
    height: 25,
    marginHorizontal: 5,
    backgroundColor: 'green',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  hapus:{
    width: 25,
    height: 25,
    marginHorizontal: 5,
    backgroundColor: 'red',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  plus :{
    backgroundColor: 'white',
    height: 50,
    width: 50,
    marginLeft: 10,
    marginBottom: 20,
    borderRadius: 50,
    elevation: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  menu:{
    backgroundColor: '#ff80ab',
    height: 60,
    flex: 1,
    paddingTop: 5,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  logo:{
    backgroundColor: '#ff80ab',
    height: 60,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  hapusall:{
    backgroundColor: '#ff80ab',
    height: 60,
    flex: 1,
    paddingRight: 15,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  cari:{
    color: 'black',
    fontFamily: 'segoeprb',
    backgroundColor: '#fce4ec',
    marginVertical: 10,
    marginHorizontal: 10,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#ff4081',
    borderRadius: 10,
  },
  textbelanja:{
    fontFamily: 'segoeprb',
    backgroundColor: 'white',
    paddingLeft: 20,
    height: 50,
    color: 'black',
    width: 320,
    marginLeft: 20,
    marginBottom: 20,
    borderRadius: 50,
    elevation: 2,
  },
  tombolcari:{
    height:35,
    width:35,
    backgroundColor:'#f5f5f5',
    position:'absolute',
    top:0,
    right:0,
    borderRadius:5,
    marginRight:18,
    marginTop:17.5,
    justifyContent:'center',
    alignItems:'center'
  },
})
