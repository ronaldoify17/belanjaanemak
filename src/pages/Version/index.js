import React from 'react'
import { StyleSheet, Text,Image,StatusBar, View } from 'react-native'

const Version = () => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <View>
        <Image
          source={require('../../assets/images/emak.png')}
          style={{
            width: 217,
            height: 70,
          }}
        />
      </View>
      <Text style={{
        position:'absolute',
        fontFamily:'OpenSans-Regular', 
        bottom:0, 
        marginBottom:35
        }}>Version 1.0</Text>
    </View>
  );
}

export default Version;

const styles = StyleSheet.create({})
