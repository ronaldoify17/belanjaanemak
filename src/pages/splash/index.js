import React, {useEffect} from 'react';
import {View, Image, StatusBar} from 'react-native';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home');
    }, 3000);
  });
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#f8bbd0',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <StatusBar backgroundColor="#f8bbd0" barStyle="dark-content" />
      <View>
        <Image
          source={require('../../assets/images/emak.png')}
          style={{
            width: 217,
            height: 70,
          }}
        />
      </View>
    </View>
  );
};

export default Splash;
