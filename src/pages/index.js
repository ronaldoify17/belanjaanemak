import Splash from './splash';
import Home from './home';
import About from './about';
import Version from './Version';

export {Splash, Home, About, Version};
