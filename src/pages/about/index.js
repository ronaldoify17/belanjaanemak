import React from 'react'
import { Image, ScrollView, StatusBar, StyleSheet, Text, View } from 'react-native'

const About = () => {
    return (
        <View style={{flex:1}}>
            <ScrollView>
                <StatusBar backgroundColor="#ff80ab"/>
                <View style={{backgroundColor:'white'}}>
                    <Image 
                    source={require('../../assets/images/back.png')}
                    style={{
                        width:'90%',
                        alignSelf:'center'
                    }}/>
                    <Image source={require('../../assets/images/doifyy.png')}
                    style={{
                        width:'90%',
                        alignSelf:'center',
                        position:'absolute',
                    }}/>
                    <Image source={require('../../assets/images/front.png')}
                    style={{
                        width:'90%',
                        alignSelf:'center',
                        position:'absolute',
                    }}/>
                </View>
                <View style={{
                    backgroundColor:'white',

                }}>
                    <Text style={{
                        fontSize:40,
                        marginTop:-30,
                        paddingLeft:20,
                        fontFamily:'Moonrising'
                    }}>Ach Dhoify</Text>
                    <Text style={{
                        fontSize:20,
                        paddingLeft:20,
                        marginTop:-5,
                        letterSpacing:3,
                        fontFamily:'Oswald-Regular',
                        color:'grey'
                    }}>Developer | Designer</Text>
                    
                </View>
                <View style={{
                    backgroundColor:'white'
                }}>
                    <View style={{
                        width:'100%',
                        marginTop:20,
                        height:320,
                        padding:20,
                        backgroundColor:'#eeeeee',
                        borderTopLeftRadius:20,
                        borderTopRightRadius:20,
                    }}>
                        <Text style={{
                            fontSize:15,
                            paddingLeft:20,
                            paddingRight:20,
                            fontFamily:'OpenSans-Regular',
                            textAlign:'justify'
                        }}>     Aplikasi ini merupakan sarana pengganti buku atau catatan dikala kita nantinya disuruh berbelanja oleh orang tua kita baik itu ibu atau bapak, tapi saking banyaknya atau saking sulitnya kita untuk mengingat belanjaan apa aja yang diperintahkan untuk dibeli, nah aplikasi ini hadir untuk mengatasi masalah tersebut, emang sudah banyak aplikasi semacam ini yg beredar luas di internet maupun playstore, tetapi aplikasi ini mengunggulkan interface yang menarik serta penggunaan yang lebih efisien dan juga elegan</Text>
                        <Text style={{
                            fontSize:20,
                            paddingLeft:10,
                            marginTop:30,
                            color:'black',
                            letterSpacing:1,
                            fontFamily:'EnjoiCrafter'
                        }}>Follow Us</Text>
                        </View>
                    <View style={{
                        flexDirection:'row',
                        position:'absolute',
                        bottom:10,
                        padding:20
                    }}>
                        <View style={{
                            width:'100%',
                            height:30,
                            marginHorizontal:5,
                            flex:1,
                            flexDirection:'row',
                            borderWidth:2,
                            borderColor:'grey',
                            borderRadius:10
                        }}><Image source={require('../../assets/images/fb.png')}
                        style={{
                            width:26, 
                            height:26, 
                            borderRadius:5, 
                            borderTopRightRadius:0,
                            borderBottomRightRadius:0
                        }}/>
                        <Text style={{
                            alignSelf:'center',
                            marginLeft:10,
                            fontSize:18,
                            letterSpacing:1,
                            fontFamily:'Oswald-Regular'
                        }}>Ronaldoify</Text>
                        </View>
                        <View style={{
                            width:'100%',
                            height:30,
                            marginHorizontal:5,
                            flex:1,
                            flexDirection:'row',
                            borderWidth:2,
                            borderColor:'grey',
                            borderRadius:10
                        }}>
                            <Image source={require('../../assets/images/call.png')}
                            style={{
                                width:26, 
                                height:26, 
                                borderRadius:5, 
                                borderTopRightRadius:0,
                                borderBottomRightRadius:0
                            }}/>
                            <Text style={{
                                alignSelf:'center',
                                marginLeft:10,
                                fontSize:18,
                                fontFamily:'Oswald-Regular'
                            }}>+62 852 3453 6311</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default About;

const styles = StyleSheet.create({})
